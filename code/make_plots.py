import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def make_plots():

    # Get data from csv file
    data = pd.read_csv("../output/500,1000,.000016,.005,medium_gap_pdf,GTR+second,25,2.0,1.0/R0/sp_score.csv")

    iter_lim = data.groupby(['iter_limit'])
    sub_algn_len = data.groupby(['sub_algn_len'])

    plt.figure(1)
    sns.lineplot(y="sp_score", x="sub_algn_len", data=data, hue="iter_limit", palette=sns.color_palette("muted", n_colors=3))
    plt.ylabel("SP Score")
    plt.xlabel("Subset alignment size")
    plt.legend(title="Number of iterations")
    plt.figure(2)
    sns.lineplot(y="sp_score", x="iter_limit", data=data, hue="sub_algn_len")
    plt.ylabel("SP Score")
    plt.xlabel("Number of iterations")
    plt.legend(title="Subset alignment size")
    plt.show()

make_plots()