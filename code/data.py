# Add paths to data to the dictionary data. Use variable 'curdir' to declare path relative to the directory of this file.

import os

curdir = os.getcwd()
data = {}

data['500M1'] = "/../data/500,1000,.000016,.005,medium_gap_pdf,GTR+second,25,2.0,1.0"
data['500L1'] = "/../data/500,1000,.000016,.01,long_gap_pdf,GTR+second,25,2.0,1"
data['1000L1'] = "/../data/1000,1000,.0000043,.005,long_gap_pdf,GTR+second,35,2.0,1.0"
data['100S1'] = "/../data/100,1000,.0001,.01,short_gap_pdf,GTR+second,10,2.0,1"
data['1000L3'] = "/../data/1000,1000,.00001,.005,long_gap_pdf,GTR+second,30,2.0,1.0"
data['1000M1'] = "/../data/1000,1000,.0000082,.005,medium_gap_pdf,GTR+second,35,2.0,1.0"
data['1000S1'] = "/../data/1000,1000,.0000082,.005,short_gap_pdf,GTR+second,35,2.0,1.0"
