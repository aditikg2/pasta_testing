import sys
import subprocess
import time
from data import *

def submit_job(num_iter=3, algn_sub_len=200, MODL = "500L1", REPL = "R0", job_name = ""):
    
    if job_name is "":
        job_name = "job_" + MODL + "_" + REPL + "_" + str(num_iter) + "_" + str(algn_sub_len)

    cur = "/projects/tallis/aditikg2/PASTA/code/jobs"
    dat = data[MODL]
    out = data[MODL].replace('/data/', '/output/')

    data_algn = "$data/" + REPL + "/rose.ualn.true.fasta"
    out_algn = "$out/" + REPL + "/" + str(job_name) + ".marker001.rose.ualn.true.aln"
    comp = "$out/" + REPL + "/" + str(job_name) + ".comp"
    
    # Open qsub job file
    job = open("jobs/" + job_name + ".pbs", 'w+')

    # Write the qsub commands
    job.write("#!/bin/bash\n\n#PBS -N \"" + str(job_name) + "\"\n#PBS -W group_list=tallis\n#PBS -q secondary\n#PBS -l nodes=1:ppn=12\n#PBS -l walltime=03:00:00\n#PBS -j oe\n#PBS -m be\n\ncd $PBS_O_WORKDIR\n")

    # Write the paths to data, outputs, etc...
    job.write("cur=\"" + str(cur) + "\"\ndata=\"$cur/.." + str(dat) + "\"\nout=\"$cur/.." + str(out) + "\"\n\n")

    # Write the PASTA command
    job.write("sh \"$PBS_O_WORKDIR/run_pasta.pbs\" -i \"" + data_algn + "\" --iter-limit=" + str(num_iter) + " --max-subproblem-size=" + str(algn_sub_len) + " -j " + str(job_name) + " -o \"$out/" + str(REPL) + "/\" \n\n")

    # Write the FastSP commands
    job.write("sh \"$PBS_O_WORKDIR/run_fastsp.pbs\" -r \"" + str(data_algn).replace("ualn", "aln") + "\" -e \"" + out_algn + "\" -o \"" + comp + "\"\n")
    job.write("sp_score=`grep -F \'SP-Score\' \"" + comp + "\"`\n")
    job.write("sp_score=${sp_score/\'SP-Score \'/\'\'}\n")
    job.write("SPFN=`grep -F \'SPFN\' \"" + comp + "\"`\n")
    job.write("SPFN=${SPFN/\'SPFN \'/\'\'}\n")
    job.write("SPFP=`grep -F \'SPFP\' \"" + comp + "\"`\n")
    job.write("SPFP=${SPFP/\'SPFP \'/\'\'}\n")
    
    # Make a common csv files  with format job_name,MODL,REPL,iter_limit,sub_algn_len,sp_score
    job.write("printf \"" + str(job_name) + "," + MODL + "," + REPL + "," + str(num_iter) + "," + str(algn_sub_len) + ",$sp_score,$SPFN,$SPFP\\n\" >> \"$out/" + REPL + "/sp_score.csv\"")
    
    # Close the file
    job.close()
    
    # Submit job and get job_id
    os.system("qsub jobs/" + str(job_name) + ".pbs")
    #out = subprocess.Popen(["qsub", "jobs/" + str(job_name) + ".pbs"], stdout = subprocess.PIPE)
    #out = subprocess.Popen(["qsub", "test.sh"], stdout = subprocess.PIPE)
    #job_id = out.stdout.read()
    #out.wait()

    # Get timing for the job
    #job_id = str(job_id)
    #job_id = job_id.split("'")[1]
    #job_id = job_id.split("\\")[0]
    #job_id = job_id.split(".")[0]

def submit_jobs(MODL = "500L1", num_iter = 3, algn_sub_len = 200, num_repl = 10):

    for i in range(num_repl):

        REPL = "R" + str(i)
        submit_job(num_iter, algn_sub_len, MODL, REPL)
    

func = input("Enter the function number\n1:Running a single job\n2:Runnig a job on multiple replicates\n")

if func == '1':
    print("Work in Progress")
elif func == '2':
    MODL = input("Enter MODL\n")
    #MODL = "1000L3"
    #num_iter = input("Enter number of iterations\n")
    for i in (1, 2, 4, 5, 6, 7):
        print(i)
        num_iter = i
        #algn_sub_len = input("Enter sub-alignment size\n")
        algn_sub_len = 200
        #num_repl = input("Enter number of replicates\n")
        num_repl = 20
        #submit_jobs(MODL, int(num_iter), int(algn_sub_len), int(num_repl))
    for i in (2, ):
        print(i)
        num_iter = 3
        #algn_sub_len = input("Enter sub-alignment size\n")
        algn_sub_len = i
        #num_repl = input("Enter number of replicates\n")
        num_repl = 20
        submit_jobs(MODL, int(num_iter), int(algn_sub_len), int(num_repl))
