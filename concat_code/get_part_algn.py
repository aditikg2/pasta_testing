import dendropy
import sys
from data import *
import os

def get_part_algn(fname, start, length):

    dna = dendropy.DnaCharacterMatrix.get(file=open(fname), schema="fasta")

    end = start + length
    if end > dna.sequence_size:
        end = dna.sequence_size
        
    for i in range(0, len(dna)):
        
         dna[i] = dna[i][start:end]

    f = open(fname.replace("rose", "rose_" + str(start) + "_" + str(length)), 'w+')
    dna.write(file = f, schema = "fasta")
    

# For this condition arg1 = MODL, arg2 = start, arg3 = length of sequence
if len(sys.argv) == 4:

    MODL = sys.argv[1]
    start = sys.argv[2]
    length = sys.argv[3]

    for i in range(0, 20):
        path = os.getcwd() + data[MODL] + "/R" + str(i) + "/rose.ualn.true.fasta"
    
        get_part_algn(path, int(start), int(length))

else:

    print("Hint: program MODL start length")
