import sys
import dendropy
from data import *
import os

def algn_to_ualn(fname):

    dna = dendropy.DnaCharacterMatrix.get(file=open(fname), schema="fasta")

    for i in range(0, len(dna)):

        dna[i] = dna[i].symbols_as_string().replace('-', '')
    
    f = open(fname.replace("aln", "ualn"), 'w+')

    dna.write(file = f, schema = "fasta")

# For this condition arg1 = MODL                                                                                                                                                                     
if len(sys.argv) == 2:

    MODL = sys.argv[1]

    for i in range(0, 20):
        path = os.getcwd() + data[MODL] + "/R" + str(i) + "/rose.aln.true.fasta"

        algn_to_ualn(path)
else:
    
    print("Hint: program MODL")
