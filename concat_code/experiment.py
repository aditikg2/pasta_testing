import sys
import subprocess
import time
from data import *

def submit_job(num_iter=5, algn_sub_len=20, MODL = "500M1", REPL = "R0", job_name = "", start = 0, length = 100):
    
    if job_name is "":
        job_name = "job_" + MODL + "_" + REPL + "_" + str(num_iter) + "_" + str(algn_sub_len)

    cur = "/projects/tallis/aditikg2/PASTA/code/jobs"
    dat = data[MODL]
    out = data[MODL].replace('/data/', '/concat_output/')

    orig = "$data/" + REPL + "/rose.aln.true.fasta"
    data_algn = "$data/" + REPL + "/rose_" + str(start) + "_" + str(length) +  ".ualn.true.fasta"
    out_algn = "$out/" + REPL + "/" + str(job_name) + ".marker001.rose_" + str(start) + "_" + str(length) + ".ualn.true.aln"
    comp = "$out/" + REPL + "/" + str(job_name) + ".comp"
    
    # Open qsub job file
    job = open("jobs/" + job_name + ".pbs", 'w+')

    # Write the qsub commands
    job.write("#!/bin/bash\n\n#PBS -N \"" + str(job_name) + "\"\n#PBS -W group_list=tallis\n#PBS -q secondary\n#PBS -l nodes=1:ppn=12\n#PBS -l walltime=03:00:00\n#PBS -j oe\n#PBS -m be\n\ncd $PBS_O_WORKDIR\n")

    # Write the paths to data, outputs, etc...
    job.write("cur=\"" + str(cur) + "\"\ndata=\"$cur/.." + str(dat) + "\"\nout=\"$cur/.." + str(out) + "\"\n\n")

    # Write the PASTA command
    job.write("sh \"$PBS_O_WORKDIR/run_pasta.pbs\" -i \"" + data_algn + "\" --iter-limit=" + str(num_iter) + " --max-subproblem-size=" + str(algn_sub_len) + " -j " + str(job_name) + " -o \"$out/" + str(REPL) + "/\" \n\n")

    # Write the FastSP commands
    job.write("sh \"$PBS_O_WORKDIR/run_fastsp.pbs\" -r \"" + str(orig) + "\" -e \"" + out_algn + "\" -o \"" + comp + "\"\n")
    job.write("sp_score=`grep -F \'SP-Score\' \"" + comp + "\"`\n")
    job.write("sp_score=${sp_score/\'SP-Score \'/\'\'}\n")
    job.write("SPFN=`grep -F \'SPFN\' \"" + comp + "\"`\n")
    job.write("SPFN=${SPFN/\'SPFN \'/\'\'}\n")
    job.write("SPFP=`grep -F \'SPFP\' \"" + comp + "\"`\n")
    job.write("SPFP=${SPFP/\'SPFP \'/\'\'}\n")
    
    # Make a common csv files  with format job_name,MODL,REPL,iter_limit,sub_algn_len,sp_score
    job.write("printf \"" + str(job_name) + "," + MODL + "," + REPL + "," + str(num_iter) + "," + str(algn_sub_len) + ",$sp_score,$SPFN,$SPFP\\n\" >> \"$out/" + REPL + "/sp_score.csv\"")
    
    # Close the file
    job.close()
    
    # Submit job and get job_id
    os.system("qsub jobs/" + str(job_name) + ".pbs")
    #out = subprocess.Popen(["qsub", "jobs/" + str(job_name) + ".pbs"], stdout = subprocess.PIPE)
    #out = subprocess.Popen(["qsub", "test.sh"], stdout = subprocess.PIPE)
    #job_id = out.stdout.read()
    #out.wait()

    # Get timing for the job
    #job_id = str(job_id)
    #job_id = job_id.split("'")[1]
    #job_id = job_id.split("\\")[0]
    #job_id = job_id.split(".")[0]

def submit_jobs(MODL = "500L1", num_iter = 5, algn_sub_len = 20, num_repl = 20):

    for i in range(num_repl):

        REPL = "R" + str(i)
        submit_job(num_iter, algn_sub_len, MODL, REPL, "", 0, 100)
    
if len(sys.argv) == 2:

    MODL = sys.argv[1]
    
    submit_jobs(MODL)
